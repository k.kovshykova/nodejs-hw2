const mongoose = require('mongoose');

const noteSchema = mongoose.Schema({
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
  },
  completed: {
    type: Boolean,
    default: false,
  },
  text: {
    type: String,
    required: true,
  },
  createdDate: {
    type: Date,
    default: () => Date.now(),
  },
});

const Note = mongoose.model('note', noteSchema);

module.exports = {
  Note,
};
