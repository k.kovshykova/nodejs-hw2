const express = require('express');

const router = express.Router();
const {
  createNote,
  getMyNotes,
  getMyNoteById,
  updateMyNoteById,
  markMyNoteCompletedById,
  deleteNote,
} = require('../controllers/notesService');
const { authMiddleware } = require('../middleware/authMiddleware');

router.get('/', authMiddleware, getMyNotes);

router.post('/', authMiddleware, createNote);

router.get('/:id', authMiddleware, getMyNoteById);

router.put('/:id', authMiddleware, updateMyNoteById);

router.patch('/:id', authMiddleware, markMyNoteCompletedById);

router.delete('/:id', deleteNote);

module.exports = {
  notesRouter: router,
};
