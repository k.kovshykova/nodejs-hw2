const { Note } = require('../models/Notes');

async function createNote(req, res) {
  const { userId } = req.user;
  const { text } = req.body;
  const note = new Note({
    userId,
    text,
  });
  await note.save().then((result) => {
    res.status(200).send({ message: 'Success', note: result });
  });
}

const getMyNotes = async (req, res) => {
  await Note.find({ userId: req.user.userId }, '-__v').then((result) => {
    res.status(200).send({ message: 'Success', notes: result });
  });
};

const getMyNoteById = async (req, res) => {
  const { id } = req.params;
  return Note.findById(id)
    .then((result) => {
      res.status(200).send({ message: 'Success', note: result });
    })
    .catch(() => {
      res.status(400).send('Error');
    });
};

const updateMyNoteById = async (req, res) => {
  const { text } = req.body;
  const result = await Note.findByIdAndUpdate(
    { _id: req.params.id, userId: req.user.userId },
    { $set: { text } },
  );
  res.status(200).send({ message: 'Success', note: result });
};

const markMyNoteCompletedById = async (req, res) => {
  const note = await Note.findById(req.params.id);
  let message;
  let markCompleted;
  if (note.completed) {
    markCompleted = false;
    message = 'Note was marked uncompleted';
  } else {
    markCompleted = true;
    message = 'Note was marked completed';
  }
  await Note.updateOne(
    { _id: req.params.id },
    { $set: { completed: markCompleted } },
  );

  res.status(200).send({ message });
};

const deleteNote = async (req, res) => {
  await Note.findByIdAndDelete(req.params.id).then(() => {
    res.status(200).send({
      message: `Note with id: ${req.params.id} was deleted`,
    });
  });
};

module.exports = {
  createNote,
  getMyNotes,
  getMyNoteById,
  updateMyNoteById,
  markMyNoteCompletedById,
  deleteNote,
};
