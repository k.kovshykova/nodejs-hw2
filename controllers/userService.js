const bcrypt = require('bcryptjs');
const { User } = require('../models/Users');

const getUserInfo = async (req, res) => {
  try {
    await User.findById(req.user.userId).then((user) => {
      res.status(200).send({ user });
    });
  } catch (err) {
    res.status(400).send('Error');
  }
};

const changeUserPassword = async (req, res) => {
  const { oldPassword, newPassword } = req.body;
  const user = await User.findOne({ username: req.user.username });
  console.log(await bcrypt.compare(String(oldPassword), String(user.password)));
  console.log(user);
  if (await bcrypt.compare(String(oldPassword), String(user.password))) {
    await User.updateOne(
      { username: req.user.username },
      { $set: { password: await bcrypt.hash(newPassword, 10) } },
    );
    res.status(200).send({ message: 'Success' });
  } else {
    res.status(400).send({ message: 'Error' });
  }
};

const deleteUser = async (req, res) => {
  try {
    await User.findByIdAndDelete(req.user.userId).then(() => {
      res.status(200).send({ message: 'Success' });
    });
  } catch (err) {
    res.status(400).send('Error');
  }
};

module.exports = {
  getUserInfo,
  changeUserPassword,
  deleteUser,
};
